# Test Project - GraphQL client

This is a project created as an example of a GraphQL client for the project [Test Project](https://gitlab.com/rios.martinivan/test-project).

## Table of contents
- [How to run the project](#how-to-run-the-project)
- [Authors and acknowledgment](#authors-and-acknowledgment)

***

## How to run the project

To run the project, you need to have NodeJS / Yarn installed on your system.

If you have NVM installed, you can run the following command to select the correct NodeJS version:
```bash
nvm use
```

Before running it, you need to download all the dependencies and build the project, you can do it
with the following commands:
```bash
yarn install
yarn build
```

To finally run the project, you can execute the following command:
```bash
yarn start
```

## Authors and acknowledgment

- [Martin Ivan Rios](https://linkedin.com/in/ivr2132)
