import { ApolloClient, InMemoryCache, gql, NormalizedCacheObject, DocumentNode } from "@apollo/client/core"

const client: ApolloClient<NormalizedCacheObject> = new ApolloClient({
  uri: "http://localhost:9090/graphql",
  cache: new InMemoryCache(),
})

type Variables = {
  [name: string]: string
}
function runQuery(query: DocumentNode, variables: Variables | undefined = undefined) {
  client
    .query({
      query: query,
      variables: variables,
    })
    .then((response) => console.log(JSON.stringify(response.data)))
    .catch((err) => console.error("Error", err))
}

function runMutation(mutation: DocumentNode, variables: Variables | undefined = undefined) {
  client
    .mutate({
      mutation: mutation,
      variables: variables,
    })
    .then((response) => console.log(JSON.stringify(response.data)))
    .catch((err) => console.error("Error", err))
}

// // Comments
const createComment = gql`
  mutation CreateComment($content: String!, $authorId: ID!, $postId: ID!) {
    createComment(comment: { content: $content }, authorId: $authorId, postId: $postId)
  }
`
runMutation(createComment, { content: "test", authorId: "1", postId: "1" })

const updateComment = gql`
  mutation UpdateComment($id: ID!, $content: String!) {
    updateComment(id: $id, comment: { content: $content })
  }
`
runMutation(updateComment, { id: "1", content: "test" })

const deleteComment = gql`
  mutation DeleteComment($id: ID!) {
    deleteComment(id: $id)
  }
`
runMutation(deleteComment, { id: "30" })
///////

// Posts
const allPosts = gql`
  {
    posts {
      title
      author {
        name
      }
    }
  }
`
runQuery(allPosts)

const postById = gql`
  query PostById($id: ID!) {
    postById(id: $id) {
      title
      content
      author {
        name
      }
      comments(page: 0, size: 100000) {
        content
        author {
          name
        }
      }
    }
  }
`
runQuery(postById, { id: "1" })

const createPost = gql`
  mutation CreatePost($title: String!, $content: String!, $authorId: ID!) {
    createPost(post: { title: $title, content: $content }, authorId: $authorId)
  }
`
runMutation(createPost, { title: "test", content: "test", authorId: "1" })

const updatePost = gql`
  mutation UpdatePost($id: ID!, $title: String!, $content: String!) {
    updatePost(id: $id, post: { title: $title, content: $content })
  }
`
runMutation(updatePost, { id: "1", title: "test", content: "test" })

const deletePost = gql`
  mutation DeletePost($id: ID!) {
    deletePost(id: $id)
  }
`
runMutation(deletePost, { id: "20" })
//////

// Users
const allUsers = gql`
  {
    users {
      name
      email
    }
  }
`
runQuery(allUsers)

const createUser = gql`
  mutation CreateUser($name: String!, $email: String!) {
    createUser(user: { name: $name, email: $email })
  }
`
runMutation(createUser, { name: "test", email: `${Math.random().toString(36).slice(2, 12)}@email.com` })

const updateUser = gql`
  mutation UpdateUser($id: ID!, $name: String!, $email: String!) {
    updateUser(id: $id, user: { name: $name, email: $email })
  }
`
runMutation(updateUser, { id: "1", name: "test", email: `${Math.random().toString(36).slice(2, 12)}@email.com` })

const deleteUser = gql`
  mutation DeleteUser($id: ID!) {
    deleteUser(id: $id)
  }
`
runMutation(deleteUser, { id: "10" })
//////
